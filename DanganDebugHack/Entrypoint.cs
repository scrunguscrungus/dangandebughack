﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using EasyHook;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting.Channels;
using System.Collections;
using DanganDebugHack.Bridges;
using Binarysharp.MemoryManagement;

namespace DanganDebugHack
{
	[Serializable]
	public class Entrypoint : IEntryPoint
	{
		enum Dangan_Memory : int
		{

			//NOT ACTUALLY ANYTHING TO DO WITH FILE LOADING
			//Some kind of general string related crap
			//PROC_LOADFILE = 0xFD6A0,

			PROC_PLAYMOVIE = 0x0D65C0,
			PROC_DEBUGPRINT = 0x100B00
		}

		enum Dangan_Value_Type
		{
			VAL_TYPE_INT = 0,
			VAL_TYPE_FLOAT = 1,
			VAL_TYPE_STRING = 2,
		}

		InjectorBridge bridge = null;
		ClientCaptureInterfaceEventProxy _clientEventProxy = new ClientCaptureInterfaceEventProxy();
		IpcServerChannel _clientServerChannel = null;

		Queue<string> messageQueue = new Queue<string>();

		List<LocalHook> hooks = new List<LocalHook>();

		class DanganNotableValue
		{
			public DanganNotableValue(string name, int address, Dangan_Value_Type type)
			{
				this.name = name;
				this.address = address;
				this.type = type;
			}

			public string name;
			public int address;
			public Dangan_Value_Type type;
		}

		List<DanganNotableValue> DR1NotableAddresses = new List<DanganNotableValue>
		{
			new DanganNotableValue("room_id",       0x002A5C10,		Dangan_Value_Type.VAL_TYPE_INT),

			new DanganNotableValue("player_pos_y",	0x0033CC80,		Dangan_Value_Type.VAL_TYPE_FLOAT),
			new DanganNotableValue("player_pos_z",	0x0033CC84,		Dangan_Value_Type.VAL_TYPE_FLOAT),
			new DanganNotableValue("player_pos_x",	0x0033CC88,		Dangan_Value_Type.VAL_TYPE_FLOAT),
		};

		MemorySharp MemManager;

		private IntPtr CalcRealAddress(int address)
		{
			int pid = RemoteHooking.GetCurrentProcessId();
			Process proc = Process.GetProcessById(pid);

			IntPtr hookAddress = IntPtr.Add(proc.MainModule.BaseAddress, address);
			return hookAddress;
		}
		private IntPtr CalcRealAddress(Dangan_Memory address)
		{
			return CalcRealAddress((int)address);
		}

		#region PlayMovie Hook
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		delegate void PlayMovie_Delegate(int movieNumber);

		void PlayMovie_Hook(int movieNumber)
		{
			bridge.Print($"Game wants to play movie {movieNumber}");

			IntPtr hookAddress = CalcRealAddress(Dangan_Memory.PROC_PLAYMOVIE);

			Marshal.GetDelegateForFunctionPointer<PlayMovie_Delegate>(hookAddress)(movieNumber);
		}
		#endregion

		#region DebugPrint Hook

		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		delegate void DebugPrint_Delegate(string format, IntPtr args);

		void DebugPrint_Hook(string format, IntPtr args)
		{
			bridge.Print(format);

			IntPtr hookAddress = CalcRealAddress(Dangan_Memory.PROC_DEBUGPRINT);
			Marshal.GetDelegateForFunctionPointer<DebugPrint_Delegate>(hookAddress)(format, args);
		}
		#endregion

	/*
		#region PathResolve Hook
		[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
		delegate IntPtr LoadFile_Delegate(IntPtr IOClass, string path, int unknown);

		
		IntPtr LoadFile_Hook(IntPtr AgIO, string path, int unknown)
		{
			//try
			//{
			//	string newPath = path.Replace("archive", "content");
			//	bridge.Print($"Game trying to load file {path}");
			//	bridge.Print($"Redirecting to {newPath}");
				bridge.Print($"arg2: {path}");
				IntPtr hookAddress = CalcRealAddress(Dangan_Memory.PROC_LOADFILE);
				return Marshal.GetDelegateForFunctionPointer<LoadFile_Delegate>(hookAddress)(AgIO, path, unknown);
			//}
			//catch (Exception e)
			//{
			//	bridge.Print($"{e.Message}");
			//	IntPtr hookAddress = CalcRealAddress(Dangan_Memory.PROC_LOADFILE);
			//	return Marshal.GetDelegateForFunctionPointer<LoadFile_Delegate>(hookAddress)(AgIO, path, unknown);
			//}
		}
		#endregion
	*/

		#region Connect to output window (Constructor)
		public Entrypoint( RemoteHooking.IContext context, string channel)
		{
			bridge = RemoteHooking.IpcConnectClient<InjectorBridge>(channel);
			bridge.Heartbeat(); //We're here!!!

			#region Allow client event handlers (bi-directional IPC)
			// Attempt to create a IpcServerChannel so that any event handlers on the client will function correctly
			System.Collections.IDictionary properties = new System.Collections.Hashtable();
			properties["name"] = channel;
			properties["portName"] = channel + Guid.NewGuid().ToString("N"); // random portName so no conflict with existing channels of channelName

			System.Runtime.Remoting.Channels.BinaryServerFormatterSinkProvider binaryProv = new System.Runtime.Remoting.Channels.BinaryServerFormatterSinkProvider();
			binaryProv.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

			System.Runtime.Remoting.Channels.Ipc.IpcServerChannel _clientServerChannel = new System.Runtime.Remoting.Channels.Ipc.IpcServerChannel(properties, binaryProv);
			System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel(_clientServerChannel, false);
			#endregion

		}
		#endregion

		#region Procedure hooking by address
		private void HookProc(int address, Delegate del)
		{
			try
			{
				IntPtr hookAddress = CalcRealAddress(address);

				bridge.Print($"Hooking function at {hookAddress.ToString("X")}");
				LocalHook newHook = LocalHook.Create(hookAddress, del, null);
				hooks.Add(newHook);
			}
			catch ( Exception e )
			{
				bridge.Print($"An exception occurred when trying to hook 0x{address.ToString("X")} (Rebased: 0x{CalcRealAddress(address).ToString("X")}): {e.Message}");
			}

		}
		private void HookProc(Dangan_Memory address, Delegate del)
		{
			HookProc((int)address, del);
		}
		#endregion
		#region Procedure hooking by module and proc name
		private void HookProc(string module, string procname, Delegate del)
		{
			bridge.Print($"Hooking function {procname} from  {module}");
			LocalHook newHook = LocalHook.Create(LocalHook.GetProcAddress(module, procname), del, null);
			hooks.Add(newHook);
		}
		#endregion

		//[DllImport("kernel32.dll", SetLastError = true)]
		//static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

		public void InstallHooks()
		{
			HookProc(Dangan_Memory.PROC_PLAYMOVIE, new PlayMovie_Delegate(PlayMovie_Hook));
			//HookProc(Dangan_Memory.PROC_DEBUGPRINT, new DebugPrint_Delegate(DebugPrint_Hook));
			//HookProc(Dangan_Memory.PROC_LOADFILE, new LoadFile_Delegate(LoadFile_Hook));
			

			foreach ( LocalHook hook in hooks )
			{
				hook.ThreadACL.SetExclusiveACL(new int[] { 0 });
			}
		}

		public void DisposeHooks()
		{
			foreach ( LocalHook hook in hooks )
			{
				hook.Dispose();
			}
			LocalHook.Release();
		}

		public void Run(RemoteHooking.IContext context, string channel)
		{
			bridge.OnSuccessfullyHooked();

			try
			{
				bridge.CommandEntered += _clientEventProxy.CommandEnteredProxyHandler;

				_clientEventProxy.CommandEntered += (commandString) =>
				{
					CommandReceived(commandString);
				};
			}
			catch (Exception e)
			{
				bridge.Print($"Exception: {e.Message}");
			}

			MemManager = new MemorySharp(RemoteHooking.GetCurrentProcessId());

			//Install hooks
			InstallHooks();

			RemoteHooking.WakeUpProcess();

			try
			{
				// Loop until FileMonitor closes (i.e. IPC fails)
				while (true)
				{
					System.Threading.Thread.Sleep(500);

					string[] queued = null;

					lock (messageQueue)
					{
						queued = messageQueue.ToArray();
						messageQueue.Clear();
					}

					// Send newly monitored file accesses to FileMonitor
					if (queued != null && queued.Length > 0)
					{
						bridge.Print(queued);
					}
					else
					{
						bridge.Heartbeat();
					}
				}
			}
			catch
			{
				// Ping() or ReportMessages() will raise an exception if host is unreachable
			}

			DisposeHooks();

			MemManager.Dispose();
		}

		private void CommandReceived(string commandString)
		{
			string[] cmdSplit;
			cmdSplit = commandString.Split(' ');

			if ( cmdSplit[0] == "getvar" )
			{
				string varToGet = cmdSplit[1];

				var val = from p in DR1NotableAddresses
						  where p.name == varToGet
						  select p;

				if (val.Count() < 1)
				{
					bridge.Print("Unknown var");
					return;
				}


				IntPtr realAddress = CalcRealAddress(val.First().address);

				switch (val.First().type)
				{
					case Dangan_Value_Type.VAL_TYPE_INT:
						int outValInt = MemManager.Read<int>(realAddress, false);
						bridge.Print($"{varToGet}: {outValInt}");
						break;
					case Dangan_Value_Type.VAL_TYPE_FLOAT:
						float outValFloat = MemManager.Read<float>(realAddress, false);
						bridge.Print($"{varToGet}: {outValFloat}");
						break;
					case Dangan_Value_Type.VAL_TYPE_STRING:
						string outValStr = MemManager.ReadString(realAddress, false);
						bridge.Print($"{varToGet}: {outValStr}");
						break;
				}
			}

			if (cmdSplit[0] == "setvar")
			{
				string varToGet = cmdSplit[1];
				string newVal = cmdSplit[2];

				var val = from p in DR1NotableAddresses
						  where p.name == varToGet
						  select p;

				if (val.Count() < 1)
				{
					bridge.Print("Unknown var");
					return;
				}


				IntPtr realAddress = CalcRealAddress(val.First().address);
				try
				{
					switch (val.First().type)
					{
						case Dangan_Value_Type.VAL_TYPE_INT:
							MemManager.Write<int>(realAddress, Int32.Parse(newVal), false);
							break;
						case Dangan_Value_Type.VAL_TYPE_FLOAT:
							MemManager.Write<float>(realAddress, float.Parse(newVal), false);
							break;
						case Dangan_Value_Type.VAL_TYPE_STRING:
							MemManager.WriteString(realAddress, newVal, false);
							break;
					}
				}
				catch ( Exception e )
				{
					bridge.Print($"Invalid value {newVal}, expected value of type {val.First().type.ToString()}");
					return;
				}
				
			}

			if ( cmdSplit[0] == "playmovie" )
			{
				int movieNumber = Int32.Parse(cmdSplit[1]);

				IntPtr hookAddress = CalcRealAddress(Dangan_Memory.PROC_PLAYMOVIE);

				//PlayMovie_Hook(movieNumber);
				Marshal.GetDelegateForFunctionPointer<PlayMovie_Delegate>(hookAddress)(movieNumber);
			}
		}
	}
}
