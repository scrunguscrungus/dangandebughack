﻿using EasyHook;
using System;
using System.Diagnostics;

namespace DanganDebugHack.Bridges
{
	[Serializable]
	public delegate void CommandEnteredEvent(string commandString);
	//Bridges injector and hack
	[Serializable]
	public class InjectorBridge : MarshalByRefObject
	{
		
		public event CommandEnteredEvent CommandEntered;

		public void RecvCommand(string commandString)
		{
			SafeInvokeRecvCommand(commandString);
		}

		private void SafeInvokeRecvCommand(string commandString)
		{
			if (CommandEntered == null)
				return;         //No Listeners

			CommandEnteredEvent listener = null;
			Delegate[] dels = CommandEntered.GetInvocationList();

			foreach (Delegate del in dels)
			{
				try
				{
					listener = (CommandEnteredEvent)del;
					listener.Invoke(commandString);
				}
				catch (Exception)
				{
					//Could not reach the destination, so remove it
					//from the list
					CommandEntered -= listener;
				}
			}
		}

		public void Print(string[] messages)
		{
			foreach (string message in messages)
			{
				Print(message);
			}
		}

		public void Print(string message)
		{
			Trace.WriteLine(message);
		}

		public void ReportException(Exception e)
		{
			Trace.WriteLine("The target process has reported an error:\r\n" + e.ToString());
		}

		public void OnSuccessfullyHooked()
		{
			Trace.WriteLine($"Successfully hooked into process {RemoteHooking.GetCurrentProcessId()}");
		}

		public void Heartbeat() { }

	}

	public class ClientCaptureInterfaceEventProxy : MarshalByRefObject
	{
		public event CommandEnteredEvent CommandEntered;

		public void CommandEnteredProxyHandler(string commandString)
		{
			if (CommandEntered != null)
				CommandEntered(commandString);
		}
	}
}