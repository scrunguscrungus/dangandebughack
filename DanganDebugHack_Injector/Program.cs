﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels.Ipc;
using System.Text;
using System.Threading.Tasks;
using EasyHook;

namespace DanganDebugHack_Injector
{
	class Program
	{
		static string pathCFG = "gamepath.cfg";
		static string targetEXE;

		private static DanganDebugHack.Bridges.InjectorBridge bridge;

		private static void CollectGamePath()
		{
			while ( true )
			{
				Console.WriteLine("Please enter the path to your Danganronpa 1 executable:");
				targetEXE = Console.ReadLine();
				if (!File.Exists(targetEXE))
				{
					Console.WriteLine("You have entered an invalid path.");
				}
				else
				{
					File.WriteAllText(pathCFG, targetEXE);
					break;
				}
			}
		}

		static void Main(string[] args)
		{
			int ProcessID = 0;

			string channel = null;

			bridge = new DanganDebugHack.Bridges.InjectorBridge();

			IpcServerChannel server = RemoteHooking.IpcCreateServer<DanganDebugHack.Bridges.InjectorBridge>(ref channel, System.Runtime.Remoting.WellKnownObjectMode.Singleton, bridge);

			string lib = "DanganDebugHack.dll";
			if ( !File.Exists(pathCFG) )
			{
				CollectGamePath();

			}
			else
			{
				targetEXE = File.ReadAllText(pathCFG);
				if (!File.Exists(targetEXE))
					CollectGamePath();
			}

			RemoteHooking.CreateAndInject(targetEXE, string.Empty, 0, lib, lib, out ProcessID, channel);

			string input = string.Empty;
			while ( input != "exit" )
			{
				input = string.Empty;
				input = Console.ReadLine();
				bridge.RecvCommand(input);
			}
		}
	}
}
